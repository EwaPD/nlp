from collections import OrderedDict
from zdrobnienia.diminutives_finder.external_resource_finders import SJPFinder, WikiFinder


class DiminutivesFinder(object):
    ENDINGS_SOURCE = 'endings_source'
    NOUNS_FILE_NAME = 'noun_endings.txt'
    ADJACTIVES_FILE_NAME = 'adjective_endings.txt'

    MIN_LENGTH = 3

    def __init__(self, language='pl'):
        self.noun_endings_source_path = 'zdrobnienia/diminutives_finder/%s/%s/%s' % (DiminutivesFinder.ENDINGS_SOURCE,
                                                      language, DiminutivesFinder.NOUNS_FILE_NAME)
        self.adjactive_endings_source_path = 'zdrobnienia/diminutives_finder/%s/%s/%s' \
                                             % (DiminutivesFinder.ENDINGS_SOURCE,
                                                           language, DiminutivesFinder.ADJACTIVES_FILE_NAME)
        self.noun_endings = list()
        self.adjactive_endings = list()

    def load_and_sort_endings_from_file(self, path):
        with open(path, 'r') as f:
            endings = [line.strip() for line in f.readlines()]
        endings.sort(key=lambda x: len(x), reverse=True)
        return endings

    def load_diminutives_from_file(self):
        self.noun_endings = self.load_and_sort_endings_from_file(path=self.noun_endings_source_path)
        self.adjactive_endings = self.load_and_sort_endings_from_file(path=self.adjactive_endings_source_path)

    def find_diminutives(self, text):
        diminutives = []
        adjactives_no = 0
        nouns_no = 0

        words = text.split()
        for word in words:
            is_word_an_adjective = False
            word = word.strip(" \"„”.,?!():;'")
            if len(word) >= DiminutivesFinder.MIN_LENGTH:

                for ending in self.adjactive_endings:
                    if word.endswith(ending):
                        d = {}
                        d["word"] = word
                        d["type"] = "przymiotnik"
                        d["ending"] = ending
                        diminutives.append(d)
                        adjactives_no += 1
                        break

                if not is_word_an_adjective:
                    for ending in self.noun_endings:
                        if word.endswith(ending):
                            d = {}
                            d['word'] = word
                            d['type'] = "rzeczownik"
                            d['ending'] = ending

                            is_sjp = SJPFinder.is_sjp(word, d)

                            if is_sjp == SJPFinder.DIMINUTIVE_NOT_CONFIRMED:
                                break
                            elif is_sjp == SJPFinder.DOES_NOT_APPEAR:
                                WikiFinder.search_in_wiki(word, d)

                            diminutives.append(d)
                            nouns_no += 1
                            break

        stats = dict()
        stats["nouns"] = nouns_no
        stats["adjectives"] = adjactives_no

        return {"diminutives": diminutives, "stats": stats}