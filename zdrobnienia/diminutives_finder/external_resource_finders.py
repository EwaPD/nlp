from urllib.request import urlopen
import urllib
from urllib.parse import quote
from bs4 import BeautifulSoup
import re


class SJPFinder(object):

    RESOURCE = "https://sjp.pl/"
    DOES_NOT_APPEAR = "nie występuje w słowniku"
    DIMINUTIVE_CONFIRMED = "jest zdrobnieniem"
    DIMINUTIVE_NOT_CONFIRMED = "nie jest zdrobnieniem"

    DIMINUTIVE_LIST = ["zdrobnienie", "zdrobn.", "zdrobniale", "pieszczotliwie", "dziecko", "sympatią", "młoda",
                          "mała", "mały", "małe", "niewielkie", "niewiele", "drobnej"]

    @staticmethod
    def is_sjp(word, diminutive):
        quote_page = SJPFinder.RESOURCE + quote(word)
        page = urlopen(quote_page)
        soup = BeautifulSoup(page, 'html.parser')

        if not SJPFinder.is_in_sjp(soup):
            return SJPFinder.DOES_NOT_APPEAR

        definitions = soup.findAll('p', style="margin: .5em 0; font: medium/1.4 sans-serif; max-width: 32em; ")
        for definition in definitions:
            for dim in SJPFinder.DIMINUTIVE_LIST:
                if re.findall('\\b' + dim + '\\b', definition.text, flags=re.IGNORECASE):  # dim in definition.text:
                    diminutive["explenation"] = definition.text
                    diminutive["sjp"] = quote_page
                    return SJPFinder.DIMINUTIVE_CONFIRMED

        return SJPFinder.DIMINUTIVE_NOT_CONFIRMED

    @staticmethod
    def is_in_sjp(soup):
        content = soup.findAll('p')
        for c in content:
            if SJPFinder.DOES_NOT_APPEAR in c.text:
                return False
        return True


class WikiFinder(object):

    RESOURCE = "https://pl.wiktionary.org/wiki/"

    @staticmethod
    def search_in_wiki(word, diminutive):
        quote_page = WikiFinder.RESOURCE + quote(word)
        try:
            page = urlopen(quote_page)
            soup = BeautifulSoup(page, 'html.parser')
            definitions = soup.findAll('dd')
            for definition in definitions:
                for dim in SJPFinder.DIMINUTIVE_LIST:
                    if dim in definition.text:
                        diminutive["explenation"] = definition.text
                        diminutive["wiki"] = quote_page
        except Exception:
            pass