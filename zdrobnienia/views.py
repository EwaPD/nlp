from django.shortcuts import render

from zdrobnienia.diminutives_finder.dimutives_finder import DiminutivesFinder

def index(request):
    return render(request, 'zdrobnienia/index.html')


def result(request):
    text = request.POST['text']
    diminutives_finder = DiminutivesFinder()
    diminutives_finder.load_diminutives_from_file()
    diminutives_result = diminutives_finder.find_diminutives(text=text)
    print(diminutives_result)

    context = {
        'text': text,
        'result': diminutives_result['diminutives'],
        'stats': diminutives_result['stats']
    }
    return render(request, 'zdrobnienia/result.html', context)
